﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chernikov.Denys.RobotChallange
{
    public class ChernikovAlgorithm : IRobotAlgorithm
    {
        public ChernikovAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }
        public int Round { get; private set; }
        public string Author {
            get { return "Chernikov Denys"; }
        }
        
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            int i = 2;
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            Position stationPosition = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            if (stationPosition == null)
                return null;

            if (i % 2 == 0)
            {
                if ((movingRobot.Energy > 1000) && (robots.Count < 100))
                {
                    i++;
                    return new CreateNewRobotCommand();
                }
                else if (stationPosition == movingRobot.Position)
                {
                    i++;
                    return new CollectEnergyCommand();
                }
                else
                {
                    i++;
                    return new MoveCommand() { NewPosition = stationPosition };
                }

            }
            return null;
        }
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)

                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }
        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }











        private int GetProfit(Robot.Common.Robot myRobot, Robot.Common.Robot enemyRobot)
        {
            var loss = PositionHelper.CalculateLoss(myRobot.Position, enemyRobot.Position) + 20;
            var profit = (int)(enemyRobot.Energy * 0.3);
            if (loss > myRobot.Energy)
                return int.MinValue;
            return profit - loss;
        }
        public Position MoveToCenter(Position robotPosition)
        {
            Position newPosition = new Position();

            if (robotPosition.X > 50)
                newPosition.X = robotPosition.X - 1;
            else
                newPosition.X = robotPosition.X + 1;

            if (robotPosition.Y > 50)
                newPosition.Y = robotPosition.Y - 1;
            else
                newPosition.Y = robotPosition.Y + 1;

            return newPosition;
        }
        /*private Position GetEnoughEnergyPosition(Robot.Common.Robot myRobot, IList<Robot.Common.Robot> robots, Position desiredPosition, int maxSpendEnergy)
        {
            Position previousPosition = new Position();

            while (DistanceHelper.CalculateDistance(desiredPosition, myRobot.Position) > maxSpendEnergy)
            {
                previousPosition.X = desiredPosition.X; previousPosition.Y = desiredPosition.Y;

                desiredPosition.X = (int)((desiredPosition.X + myRobot.Position.X) / 2);
                desiredPosition.Y = (int)((desiredPosition.Y + myRobot.Position.Y) / 2);

                if (previousPosition.X == desiredPosition.X && previousPosition.Y == desiredPosition.Y)
                    return null;
            }

            //TODO
            if (PositionTakenByAlly(desiredPosition, GetAllAllyRobots(desiredPosition, robots)))
            {
                if (desiredPosition.X > 0)
                    desiredPosition.X--;
                else
                    desiredPosition.Y--;
            }

            return desiredPosition;
        }*/
        private bool PositionTakenByAlly(Position position, IList<Robot.Common.Robot> allies)
        {
            return allies.Any(al => al.Position == position);
        }
        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            Round++;
        }
        /*private int GetEnergyAround(Map map, Robot.Common.Robot robot)
        {
            List<EnergyStation> energyStations = map.GetNearbyResources(robot.Position, AlgorithmValues.CollectingDistance);

            int totalEnergy = 0;

            foreach (var es in energyStations)
            {
                totalEnergy += es.Energy;
            }

            return totalEnergy;
        }*/
        /*private int GetCountOfNearbyAllyRobots(Position currentPosition, IList<Robot.Common.Robot> robots)
        {
            return GetAllAllyRobots(currentPosition, robots)
                .Where(robot =>
                 (Math.Abs(robot.Position.X - currentPosition.X) <= AlgorithmValues.CollectingDistance)
                 && (Math.Abs(robot.Position.Y - currentPosition.Y) <= AlgorithmValues.CollectingDistance))
                .Count();
        }*/
        private List<Robot.Common.Robot> GetAllAllyRobots(Position currentPosition, IList<Robot.Common.Robot> robots)
        {
            return robots.Where(robot => (robot.OwnerName == Author))
                .Where(robot => (robot.Position != currentPosition)).ToList();
        }
        private List<Robot.Common.Robot> GetAllEnemyRobots(IList<Robot.Common.Robot> robots)
        {
            return robots.Where(robot => (robot.OwnerName != Author)).ToList();
        }
        /*private Robot.Common.Robot GetBestRobotToAttack(IList<Robot.Common.Robot> robots, Robot.Common.Robot myRobot, out int profit)
        {
            List<Robot.Common.Robot> eligibleRobots = GetAllEnemyRobots(robots)
                .Where(robot => robot.Energy > AlgorithmValues.PreferablyEnergyOfEnemyRobot).ToList();

            Robot.Common.Robot bestRobotToAttack;

            if (eligibleRobots.Count < 1)
            {
                bestRobotToAttack = null;
                profit = -100;
            }
            else
            {
                bestRobotToAttack = eligibleRobots.OrderByDescending(er => CalculateProfit(myRobot, er)).First();
                profit = CalculateProfit(myRobot, bestRobotToAttack);
            }
            return bestRobotToAttack;
        }*/
        /*private int CalculateProfit(Robot.Common.Robot myRobot, Robot.Common.Robot enemyRobot)
        {
            int distanceToRobot = DistanceHelper.CalculateDistance(myRobot.Position, enemyRobot.Position);
            if (distanceToRobot > myRobot.Energy)
                return -1;
            return ((int)(enemyRobot.Energy * AlgorithmValues.StoleRateEnergyAtAttack)) -
                distanceToRobot - AlgorithmValues.EnergyNeededToAttack;
        }*/
        /*private EnergyStation GetNearestGoodEnergyStation(Robot.Common.Robot myRobot, IList<Robot.Common.Robot> robots, Map map)
        {
            var tmpStations = map.Stations
                ?.Where(station => GetCountOfNearbyAllyRobots(station.Position, robots) == 0)
                ?.Where(station => !PositionTakenByAlly(station.Position, GetAllAllyRobots(myRobot.Position, robots)));

            List<EnergyStation> es = new List<EnergyStation>();
            if (tmpStations.Count() > 0)
            {
                int prefStAround = AlgorithmValues.PreferablyStationsAround;
                while (prefStAround > 0)
                {
                    es = tmpStations
                        .Where(station => map.GetNearbyResources(station.Position, AlgorithmValues.CollectingDistance)
                        .Count() > prefStAround).ToList();

                    if (es.Count() > 0)
                        return es.OrderBy(station => DistanceHelper.CalculateDistance(myRobot.Position, station.Position))
                            .First();
                    prefStAround--;
                }
            }
            return null;
        }*/

    }
    public class PositionHelper
    {
        private static int Min2D(int x1, int x2)
        {
            int[] nums =
            {
                (int) Math.Pow(x1 - x2, 2),
                (int) Math.Pow(x1 - x2 + 100, 2),
                (int) Math.Pow(x1 - x2 - 100, 2)
            };
            return nums.Min();
        }
        public static int CalculateLoss(Position p1, Position p2)
        {
            return Min2D(p1.X, p2.X) + Min2D(p1.Y, p2.Y);
        }
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
    }
}
