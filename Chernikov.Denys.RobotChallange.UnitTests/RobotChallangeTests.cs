﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Chernikov.Denys.RobotChallange
{
    [TestClass]
    public class ChernikovAlgorithmTests
    {
        public ChernikovAlgorithm algorithm = new ChernikovAlgorithm();      

        [TestMethod]
        public void FindDistance_ReturnsValue()
        {
            //Arrange
            Position a = new Position(2, 1);
            Position b = new Position(2, 1);
            int expected = 0;
            //Act
            int actual = DistanceHelper.FindDistance(a, b);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CalculateLoss_ReturnsValue()
        {
            Position a = new Position(2, 1);
            Position b = new Position(2, 1);
            int expected = 0;

            int actual = PositionHelper.CalculateLoss(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MoveToCenter_ReturnsPosition()
        {
            Position robot = new Position(2, 1);
            Position expected = new Position(3, 2);

            Position actual = algorithm.MoveToCenter(robot);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Author_ReturnsName()
        {
            string expected = "Chernikov Denys";

            string actual = algorithm.Author;

            Assert.AreEqual(expected, actual);
        }
    }
}
